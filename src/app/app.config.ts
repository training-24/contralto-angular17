import { provideHttpClient, withInterceptors } from '@angular/common/http';
import { ApplicationConfig } from '@angular/core';
import { provideRouter, withComponentInputBinding } from '@angular/router';
import { environment } from '../environments/environment';

import { routes } from './app.routes';
import { SuperUsersService } from './features/home/services/super-users.service';
import { UsersService } from './features/home/services/users.service';

export const appConfig: ApplicationConfig = {
  providers: [
    provideHttpClient(
      withInterceptors([])
    ),
    provideRouter(routes, withComponentInputBinding()),

    {
      provide: UsersService,
      useFactory: () => {
        return environment.production ?
          new UsersService() :
          new SuperUsersService();
      },
      deps: []
    }
  ]
};
