import { Routes } from '@angular/router';

export const routes: Routes = [
  { path: 'catalog', loadComponent: () => import('./features/catalog/catalog.component')},
  {
    path: 'catalog/:productId',
    loadComponent: () => import('./features/product.component'),
    data: { title: 'ciaone!!!!'}
  },
  { path: 'contacts', loadComponent: () => import('./features/contacts.component')},
  { path: 'home', loadComponent: () => import('./features/home/home.component')},
  { path: 'demo1', loadComponent: () => import('./features/demo1.component')},
  { path: 'demo2', loadComponent: () => import('./features/demo2.component')},
  { path: 'demo3', loadComponent: () => import('./features/demo3.component')},
  { path: '', redirectTo: 'catalog', pathMatch: 'full'}
];
