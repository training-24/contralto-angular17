import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  users = [1, 2, Math.random()]

  constructor() {
    console.log('users service constructor')
  }

  getAll() {
    // http....get
    this.users = [4, 5, 6]
  }
}
