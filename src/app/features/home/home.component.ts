import { NgClass, NgIf } from '@angular/common';
import { Component, computed, effect, inject, signal, untracked } from '@angular/core';
import { WidgetComponent } from '../../shared/components/widget.component';
import { SuperUsersService } from './services/super-users.service';
import { UsersService } from './services/users.service';


@Component({
  selector: 'app-home',
  standalone: true,
  imports: [
    NgIf,
    NgClass,
    WidgetComponent
  ],
  template: `
    
    <h1 [style.color]="getColor()">{{counter()}}</h1>
    
    
    @if (isZero()) {
      <div>il counter è zero</div>
    } 
    
    <button (click)="inc()">+</button>
    <button (click)="reset()">reset</button>
    <hr>
    <input type="text" (input)="updateText(input.value)" #input>
    {{text()}}

    <hr>
    <h1>Widgets</h1>
    
    <app-widget />
    <hr>
    <app-widget />
    <hr>
    <app-widget />
    <hr>
    
  `,
  providers: [
    // UsersService
    { provide: UsersService, useClass: SuperUsersService}
  ]
})
export default class HomeComponent {
  text = signal('')
  counter = signal(0)
  isZero = computed(() =>  this.counter() === 0)
  getColor = computed(() => this.isZero() ? 'red' : 'green')

  constructor() {
    effect(() => {
      console.log('effect counter', untracked(() => this.text()))
      localStorage.setItem('counter', this.counter().toString())
      this.text.set(this.counter().toString())
    }, { allowSignalWrites: true } );

    effect(() => {
      // console.log('effect text', this.text())
    });

  }

  inc() {
    this.counter.update(c  => c + 1)
  }
  reset() {
    this.counter.set(0)
  }
  //
  // isZero() {
  //   console.log('isZero')
  //   return this.counter() === 0;
  // }

  updateText(text: string) {

    this.text.set(text)
  }

}
