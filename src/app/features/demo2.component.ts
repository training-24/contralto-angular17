import { Component, model, signal } from '@angular/core';
import { LeafletComponent } from '../shared/components/leaflet.component';
import { LeafletSignalComponent } from '../shared/components/leafletSignal.component';
import { PanelComponent } from '../shared/panel.component';

@Component({
  selector: 'app-demo2',
  standalone: true,
  imports: [
    LeafletComponent,
    PanelComponent,
    LeafletSignalComponent
  ],
  template: `
    PARENT: {{ count }}
    
    <app-panel 
      [value]="count"
      [(value2)]="count"
    />
    
    <button (click)="count = count + 1">+</button>
    
    <hr>
    <h1>Leaflet</h1>
    <app-leaflet-signal  [coords]="coords" [zoom]="zoom" />
    <app-leaflet [coords]="coords" [zoom]="zoom" />
    
    <button (click)="coords = [44, 14]">Coords 1</button>
    <button (click)="coords = [43.2, 12]">Coords 2</button>
    <button (click)="coords = [41, 13]">Coords 3</button>
    <button (click)="zoom = zoom  - 1 ">-</button>
    <button (click)="zoom = zoom  + 1 ">+</button>
   
  `,
  styles: ``
})
export default  class Demo2Component {
  count = 123;
  zoom = 10;


  coords: [number, number] = [43, 13]
  coords2 = signal<[number, number]>([43, 13])
}
