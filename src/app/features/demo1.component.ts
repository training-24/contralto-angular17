import { Component } from '@angular/core';
import { CardComponent } from '../shared/card.component';
import { WeatherComponent } from '../shared/components/weather.component';
import { Weather2Component } from '../shared/components/weather2.component';

@Component({
  selector: 'app-demo1',
  standalone: true,
  template: `
    <h1>Weather</h1>
    
    <button (click)="value = 'trieste'">Trieste</button>
    <button (click)="value = 'Palermo'">Palermo</button>
    <app-weather
      [city]="value"
      unit="metric"
    />  
    
    <app-weather2
      [city]="value"
    />
    
    <hr>
    <h1>Card Example</h1>

    <div class="flex flex-col gap-3">
      <app-card 
        title="ciaone!"
        icon="♥️" 
        btnLabel="VISIT" 
        (btnClick)="doSomething($event)"
        (iconClick)="doSomethingElse()"
        showFooter
        value="1"
      >
        <input type="text">
        <input type="text">
        <input type="text">
      </app-card>
      
      <app-card 
        icon="💩" title="One" 
      >
        other componenti...
      </app-card>
      
      
     
    </div>
  `,
  imports: [
    CardComponent,
    Weather2Component,
    WeatherComponent
  ],
  styles: ``
})
export default class Demo1Component {
  value = 'Milan';

  doSomething(val: number) {
    window.alert('doSomething' + val)
  }

  doSomethingElse() {
    window.alert('doSomethingElse')

  }
}
