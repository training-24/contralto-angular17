import { AsyncPipe, JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, inject, Input } from '@angular/core';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { mergeMap } from 'rxjs';

@Component({
  selector: 'app-products',
  standalone: true,
  imports: [
    RouterLink,
    JsonPipe,
    AsyncPipe
  ],
  template: `
    <p>
      products works! {{productId}} - {{title}}
    </p>
    
    <button routerLink="../2">2</button>
    <button routerLink="/catalog/3">3</button>
    <button routerLink="../4">4</button>
    
    <pre>{{user$ | async | json}}</pre>
  `,
  styles: ``
})
export default class ProductComponent {

  @Input() productId: string | undefined;
  @Input() title: string | undefined;

  activatedRoute = inject(ActivatedRoute)
  http = inject(HttpClient)

  user$ = this.activatedRoute.params
    .pipe(
      mergeMap(params => this.http.get<User>(`https://jsonplaceholder.typicode.com/users/${params['productId']}`))
    )

}

interface User {
  id: number;
  name: string;
}
