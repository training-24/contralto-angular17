import { Component } from '@angular/core';

@Component({
  selector: 'app-demo3',
  standalone: true,
  imports: [],
  template: `
    <h1>worker</h1>
  `,
  styles: ``
})
export default  class Demo3Component {
  worker!: Worker

  constructor() {
    if (typeof Worker !== 'undefined') {
      this.worker = new Worker(new URL('../timer.worker', import.meta.url), { type: 'module' });

      this.worker.onmessage = ({  data  }) => {
        console.log('counter!!!', data)
      }

      this.worker.postMessage({ action: 'startTimer', payload: 0})
    }
  }
}
