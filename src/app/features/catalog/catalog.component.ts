import { HttpClient } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import { CounterService } from '../../core/counter.service';
import { CatalogFormComponent } from './components/catalog-form.component';
import { CatalogListComponent } from './components/catalog-list.component';

@Component({
  selector: 'app-catalog',
  standalone: true,
  imports: [
    CatalogFormComponent,
    CatalogListComponent
  ],
  template: `
    <p>
      catalog works! {{counterSrv.value}}
    </p>
    
    <app-catalog-form />
    <app-catalog-list />
    
    <input type="text" (keydown)="doNOthing()">
    <button (click)="inc()">inc</button>
  `,
  styles: ``
})
export default class CatalogComponent {
  http = inject(HttpClient)
  counterSrv = inject(CounterService)

  constructor() {

/*
    setInterval(() => {
      this.counter ++
    }, 1000)*/
  }

  inc() {
    this.http.get('')
      .subscribe()
    this.counterSrv.value++;
  }

  doNOthing() {
    console.log('do nothing')
  }
}
