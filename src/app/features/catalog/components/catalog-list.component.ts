import { ApplicationRef, ChangeDetectionStrategy, ChangeDetectorRef, Component, inject, Input } from '@angular/core';
import { CounterService } from '../../../core/counter.service';

@Component({
  selector: 'app-catalog-list',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <p>
      catalog-list works! {{counterSrv.value}}
    </p>
    
    1000 list item
    <!--{{render()}}-->
  `,
  imports: [],
  styles: ``
})
export class CatalogListComponent {
  cd = inject(ChangeDetectorRef)
  counterSrv = inject(CounterService)


  constructor() {
  /*  this.cd.detach()
    setTimeout(() => {
      this.cd.reattach();
    }, 4000)*/
    // mount ....
    // title ....
  }

  render() {
    console.log('render catalog list')
  }
}
