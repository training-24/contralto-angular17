import { AsyncPipe, JsonPipe, NgForOf } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, computed, inject, signal } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { catchError, delay, interval, map, of } from 'rxjs';

@Component({
  selector: 'app-contacts',
  standalone: true,
  imports: [
    JsonPipe,
    AsyncPipe,
    NgForOf
  ],
  template: `
    <p>
      contacts works!
    </p>
    
    TOTAL: {{totalUsers()}} 
    @for (user of usersSignal(); track user.id) {
      <li>
        {{user.id}} - {{user.name}}
        
        <button (click)="deleteUser(user)">delete</button>
      </li>
    } @empty {
      <div>no users</div>
    }
    <hr>
    TOTAL COST: {{totalCost()}}



    <div class="max-w-sm p-6 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
      <a href="#">
        <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">Noteworthy technology acquisitions 2021</h5>
      </a>
      <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">Here are the biggest enterprise technology acquisitions of 2021 so far, in reverse chronological order.</p>
      <a href="#" class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
        Read more
        <svg class="rtl:rotate-180 w-3.5 h-3.5 ms-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 10">
          <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 5h12m0 0L9 1m4 4L9 9"/>
        </svg>
      </a>
    </div>

  `,
  styles: ``
})
export default class ContactsComponent {
  http = inject(HttpClient);
  usersSignal = signal<User[]>([])
  totalUsers = computed(() => this.usersSignal().length)
  totalCost = computed(() => this.usersSignal().reduce((total, item) => total + item.id, 0))

  constructor() {
    this.http.get<User[]>(`https://jsonplaceholder.typicode.com/users/`)
      .subscribe(res => {
        this.usersSignal.set(res)
      })
  }

  deleteUser(userToDelete: User) {
    this.http.delete(`https://jsonplaceholder.typicode.com/users/${userToDelete.id}`)
      .subscribe(() => {
        this.usersSignal.update(
          users => users.filter(u => u.id !== userToDelete.id)
        )
      })
  }
  addUser(userToAdd: User) {
    this.http.post<User>(`https://jsonplaceholder.typicode.com/users/`, {})
      .subscribe((newUser) => {
        this.usersSignal.update(
          users => [...users, newUser]
        )
      })
  }
}

interface User {
  id: number;
  name: string;
}
