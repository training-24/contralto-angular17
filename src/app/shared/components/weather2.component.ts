import { JsonPipe, NgIf } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import {
  Component,
  computed,
  inject,
  Input,
  OnChanges,
  OnInit,
  signal,
  SimpleChange,
  SimpleChanges
} from '@angular/core';
import { Meteo } from '../../../models/meteo';

@Component({
  selector: 'app-weather2',
  standalone: true,
  imports: [
    JsonPipe,
    NgIf
  ],
  template: `
    <p>weather works! </p>
    
    @if(meteo(); as mto) {
      {{temperature()}} °
    }
  `,
})
export class Weather2Component {
  http = inject(HttpClient)

  @Input() set city(val: string) {
    this.http.get<Meteo>(`https://api.openweathermap.org/data/2.5/weather?q=${val}&units=${this.unit}&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
      .subscribe(res => {
        this.meteo.set(res)
      })
  }

  @Input()
  unit: 'metric' | 'imperial' = 'metric'

  meteo = signal<Meteo | null>(null);
  temperature = computed(() => this.meteo()?.main.temp)

/*
  ngOnChanges(changes: SimpleChanges) {
    this.http.get<Meteo>(`https://api.openweathermap.org/data/2.5/weather?q=${this.city}&units=${this.unit}&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
      .subscribe(res => {
        this.meteo.set(res)
      })
  }*/


}
