import {
  AfterViewChecked,
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild
} from '@angular/core';

import L, { LatLngExpression } from 'leaflet'

@Component({
  selector: 'app-leaflet',
  standalone: true,
  imports: [],
  template: `
    <div #map class="map"></div>
  `,
  styles: `
    .map { height: 180px; }
  `
})
export class LeafletComponent implements OnChanges {
  @ViewChild('map', { static: true }) mapRef!: ElementRef<HTMLDivElement>
  @Input({ required: true }) coords!: LatLngExpression;
  @Input() zoom = 8;
  map!: L.Map

  init() {
    this.map = L.map(this.mapRef.nativeElement)
      .setView(this.coords, this.zoom);

    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(this.map);
  }

  ngOnChanges(changes: SimpleChanges) {
    const { coords, zoom } = changes;

    if (!this.map) {
      this.init()
    }

    if (coords) {
      this.map.setView(this.coords)
    }

    if (zoom) {
      this.map.setZoom(this.zoom)
    }

  }

}
