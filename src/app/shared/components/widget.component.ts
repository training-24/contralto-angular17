import { Component, inject } from '@angular/core';
import { UsersService } from '../../features/home/services/users.service';
import { WidgetService } from './widget.service';

@Component({
  selector: 'app-widget',
  standalone: true,
  imports: [],
  template: `
    <p>
      widget works! {{widgetService.counter}}
    </p>
    <button (click)="widgetService.setVAlue()">set</button>
    usersService:  {{usersService.users}}
  `,
  providers: [
    WidgetService
  ],
  styles: ``
})
export class WidgetComponent {
  usersService = inject(UsersService)
  widgetService = inject(WidgetService)
}
