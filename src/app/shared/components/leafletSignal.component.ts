import {
  AfterViewChecked,
  AfterViewInit,
  Component, effect,
  ElementRef, input,
  Input,
  OnChanges,
  SimpleChanges, untracked, viewChild,
  ViewChild
} from '@angular/core';

import L, { LatLngExpression } from 'leaflet'

@Component({
  selector: 'app-leaflet-signal',
  standalone: true,
  imports: [],
  template: `
    <div #map class="map"></div>
  `,
  styles: `
    .map { height: 180px; }
  `
})
export class LeafletSignalComponent {
  // @ViewChild('map') mapRef!: ElementRef<HTMLDivElement>
  mapRef = viewChild<ElementRef<HTMLDivElement>>('map')

  coords = input.required<LatLngExpression>()
  zoom = input(8)
  map!: L.Map

  constructor() {
    effect(() => {
      console.log('map')
      if (!this.map) {
        this.init(untracked(() => this.mapRef()!.nativeElement))
      }
    });

    effect(() => {
      console.log('coords')
      this.map.setView(this.coords())
    });

    effect(() => {
      console.log('zoom')
      this.map.setZoom(this.zoom())
    });

  }

  init(ref: HTMLDivElement) {
    this.map = L.map(ref)
      .setView(this.coords(), this.zoom());

    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(this.map);
  }
}
