import { JsonPipe, NgIf } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import {
  Component,
  computed,
  inject,
  Input,
  OnChanges,
  OnInit,
  signal,
  SimpleChange,
  SimpleChanges
} from '@angular/core';
import { Meteo } from '../../../models/meteo';

@Component({
  selector: 'app-weather',
  standalone: true,
  imports: [
    JsonPipe,
    NgIf
  ],
  template: `
    <p>weather works! {{city}}</p>
    
    @if(meteo(); as mto) {
      {{temperature()}} °
    }
  `,
})
export class WeatherComponent implements OnChanges, OnInit {
  http = inject(HttpClient)
  @Input({ required: true })
  city: string | undefined;
  @Input()
  unit: 'metric' | 'imperial' = 'metric'

  meteo = signal<Meteo | null>(null);
  temperature = computed(() => this.meteo()?.main.temp)

  constructor() {
   console.log('ctr', this.city)
  }

  ngOnChanges(changes: SimpleChanges) {
    this.http.get<Meteo>(`https://api.openweathermap.org/data/2.5/weather?q=${this.city}&units=${this.unit}&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
      .subscribe(res => {
        this.meteo.set(res)
      })
  }

  ngOnInit() {
   console.log('init', this.city)
  }


  reset() {
  }


}
