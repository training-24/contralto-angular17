import { booleanAttribute, Component, EventEmitter, Input, numberAttribute, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  standalone: true,
  imports: [],
  template: `
    <div class="w-full p-6 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
      <div>
        <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
         <span (click)="iconClick.emit()">{{icon}}</span> 
          {{ heading }} {{value}}
        </h5>
      </div>
      <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">
        <ng-content></ng-content>
      </p>
      
      @if(btnLabel) {
        <div
          (click)="btnClick.emit(123)"
          class="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
          {{btnLabel}}
        </div>  
      }
      
      @if(showFooter) {
        <hr>
        copyright Fabio Biondi  
      }
      
    </div>
    
    
  `,
  styles: ``
})
export class CardComponent {
  @Input({
    required: true,
    alias: 'title',
    transform: (val: string) => {
      return val.toUpperCase()
    }
  }) heading!: string;

  @Input() icon: string | undefined;
  @Input() btnLabel = '';
  @Output() btnClick = new EventEmitter<number>()
  @Output() iconClick = new EventEmitter()
  @Input({ transform: booleanAttribute }) showFooter: boolean = false;
  @Input({ transform: numberAttribute }) value = 0

}
