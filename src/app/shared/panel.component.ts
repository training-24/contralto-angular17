import { Component, computed, effect, input, Input, model, signal } from '@angular/core';

@Component({
  selector: 'app-panel',
  standalone: true,
  imports: [],
  template: `
    <p>
      panel works! {{value()}} - {{double()}}
    </p>
    
    <div>
      CHILD : {{value2()}}
    </div>
    <button (click)="value2.set(10)">10</button>
    <button (click)="value2.set(20)">20</button>
  `,
  styles: ``
})
export class PanelComponent {
  value = input.required<number>()
  double = computed(() => this.value() * 2)
  value2 = model(0)


  constructor() {
    effect(() => {
      console.log("make http", this.value())
    });
  }
}
