/// <reference lib="webworker" />

let counter = 0;

addEventListener('message', ({ data }) => {
  const response = `WORKER RESPONSE ${data}`;
  console.log(response)

  if (data.action === 'startTimer') {

    setInterval(() => {
      postMessage(++counter);
    }, 1000)
  }

});
