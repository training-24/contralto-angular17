import { Component, inject } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';
import { UsersService } from './features/home/services/users.service';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterLink,
    RouterOutlet
  ],
  template: `
    <div class="flex gap-1 flex-wrap">
      <button routerLink="home">home</button>
      <button routerLink="contacts">contacts</button>
      <button routerLink="catalog">catalog</button>
      <button routerLink="demo1">demo1</button>
      <button routerLink="demo2">demo2</button>
      <button routerLink="demo3">demo3</button>
      {{ usersService.users }}
    </div>
    <hr>
    
    <div class="mx-3 xl:mx-auto max-w-screen-xl">
      <router-outlet />
    </div>
  `,
  styles: [],
})
export class AppComponent {
  usersService = inject(UsersService)
}
